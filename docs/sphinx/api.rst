.. _sphinx_api:

#############
|project| API
#############

*********
|project|
*********

.. toctree::
    :maxdepth: 2

.. _micromorphic_elasto_plasticity_source:

micromorphic_elasto_plasticity.cpp
============

.. doxygenfile:: micromorphic_elasto_plasticity.cpp

micromorphic_elasto_plasticity.h
==========

.. doxygenfile:: micromorphic_elasto_plasticity.h

****************
Abaqus interface
****************

micromorphic_elasto_plasticity_umat.cpp
=================

.. doxygenfile:: micromorphic_elasto_plasticity_umat.cpp

micromorphic_elasto_plasticity_umat.h
===============

.. doxygenfile:: micromorphic_elasto_plasticity_umat.h
